/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: readonly string[] = [
  "/",
  "/archive",
  "/archive/hammerfest",
  "/archive/hammerfest/:server/users/:user_id",
  "/docs",
  "/docs/:a",
  "/docs/:a/:b",
  "/docs/:a/:b/:c",
  "/donate",
  "/forum",
  "/forum/sections/:section_id",
  "/forum/sections/:section_id/new",
  "/forum/threads/:thread_id",
  "/forum/threads/:thread_id/reply",
  "/games",
  "/legal",
  "/login",
  "/register",
  "/register/email",
  "/register/username",
  "/settings",
  "/users/:user_id"
];
