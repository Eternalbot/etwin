import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type RfcOauthAccessTokenKey = string;

export const $RfcOauthAccessTokenKey: Ucs2StringType = $Ucs2String;
