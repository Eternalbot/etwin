import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type RfcOauthRefreshTokenKey = string;

export const $RfcOauthRefreshTokenKey: Ucs2StringType = $Ucs2String;
