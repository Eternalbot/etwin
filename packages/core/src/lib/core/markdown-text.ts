import { $Ucs2String } from "kryo/lib/ucs2-string.js";

/**
 * Raw markdown content.
 */
export type MarkdownText = string;

export const $MarkdownText = $Ucs2String;
