/**
 * Twinoid access token.
 */
export type AccessToken = string;
